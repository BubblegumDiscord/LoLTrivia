FROM python:3.6-slim-jessie
RUN apt-get update
RUN apt-get install -y gcc
RUN apt-get install -y libffi-dev libssl-dev zlib1g-dev libcurl4-openssl-dev
 
RUN pip install requests
RUN pip install pyjwt
RUN pip install cassiopeia
WORKDIR /bot
ADD requirements.txt /bot
RUN apt-get install -y git
RUN pip install -r requirements.txt
VOLUME /bot

CMD [ "python", "./run.py" ]
