import requests, time, json, jwt
#from . import config
with open("/bot/config.json") as f:
    config = json.loads(f.read())


def gbal(id):
  payload: dict = {
    "uid": str(id),
    "timeIssued": round(time.time() * 1000)
  }
  token = jwt.encode(payload, config["ecokey"])
  resp = requests.get("http://" + config["eco_uri"] + "/getbal/" + token.decode("utf-8"))
  respcontent = resp.json()
  return respcontent["balance"]

def sbal(id, amount):
  payload = {
    "uid": str(id),
    "amount": amount,
    "timeIssued": int(round(time.time() * 1000))
  }
  token = jwt.encode(payload, config["ecokey"])
  resp = requests.get("http://" + config["eco_uri"] + "/setbal/" + token.decode("utf-8"))
  print(resp)

def award(id, amount):
  sbal(id, (gbal(id) + amount))
